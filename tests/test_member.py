import unittest

from flask import url_for
from werkzeug.security import generate_password_hash

from clubhub.core import create_app, db
from clubhub.models import User, Company, Employment


class BasicTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app("testing")
        self.ctx = self.app.app_context()
        self.ctx.push()
        self.client = self.app.test_client()
        db.create_all()

        self.data = {
            "email": "john@example.com",
            "firstname": "Jörg",
            "lastname": "Doe",
            "password": "PasswordSuperSafe123!",
            "password2": "PasswordSuperSafe123!",
        }

        # create member in database
        self.user = User(
            email=self.data["email"],
            firstname=self.data["firstname"],
            lastname=self.data["lastname"],
            confirmed=True,
        )
        self.user.password_hash = generate_password_hash(self.data["password"])
        db.session.add(self.user)
        db.session.commit()

        # create company in database
        self.company = Company(name="TestCompany", contributing=True)
        db.session.add(self.company)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.ctx.pop()

    def login(self, email, password):
        return self.client.post(
            url_for("auth.login"),
            data=dict(email=email, password=password),
            follow_redirects=True,
        )

    def logout(self):
        return self.client.get(url_for("auth.logout"), follow_redirects=True)

    def test_request_membership(self):
        # login
        response = self.login(self.data["email"], self.data["password"])
        text = response.get_data(as_text=True)
        assert "Membership" in text

        # cannot create membership request without affiliation
        response = self.client.get(
            url_for("membership.request_membership"), follow_redirects=True
        )
        text = response.get_data(as_text=True)
        assert "Please update your account" in text

        # set company affiliation
        self.data["company_id"] = self.company.id
        response = self.client.post(
            url_for("account.update_my_account"),
            data=self.data,
            follow_redirects=True,
        )
        db.session.commit()
        employment = Employment.query.filter_by(
            user_id=self.user.id, company_id=self.company.id
        ).first()
        assert employment

        # now can create membership request
        response = self.client.get(
            url_for("membership.request_membership"), follow_redirects=True
        )
        text = response.get_data(as_text=True)
        assert "Please update your account" not in text
