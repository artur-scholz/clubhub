import unittest

from flask import url_for

from clubhub.core import create_app, db


class BasicTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app("testing")
        self.ctx = self.app.app_context()
        self.ctx.push()
        self.client = self.app.test_client()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.ctx.pop()

    def test_redirect_to_homepage(self):
        response = self.client.get("/")
        assert "Redirecting" in response.get_data(as_text=True)

        response = self.client.get(
            url_for("home.index"), follow_redirects=True
        )
        assert "Register" in response.get_data(as_text=True)

    def test_redirect_to_login(self):
        response = self.client.get(
            url_for("account.details_my_account"), follow_redirects=True
        )
        assert "Login" in response.get_data(as_text=True)

        response = self.client.get(
            url_for("club.list_all_clubs"), follow_redirects=True
        )
        assert "Login" in response.get_data(as_text=True)

        response = self.client.get(
            url_for("company.list_all_companies"), follow_redirects=True
        )
        assert "Login" in response.get_data(as_text=True)

        response = self.client.get(
            url_for("account.list_confirmed_users"), follow_redirects=True
        )
        assert "Login" in response.get_data(as_text=True)
