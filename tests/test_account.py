import unittest

from flask import url_for
from werkzeug.security import generate_password_hash

from clubhub.core import create_app, db
from clubhub.models import User


class BasicTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app("testing")
        self.ctx = self.app.app_context()
        self.ctx.push()
        self.client = self.app.test_client()
        db.create_all()

        self.user_data = {
            "email": "john@example.com",
            "firstname": "Jörg",
            "lastname": "Doe",
            "password": "PasswordSuperSafe123!",
            "password2": "PasswordSuperSafe123!",
        }

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.ctx.pop()

    def login(self, email, password):
        return self.client.post(
            url_for("auth.login"),
            data=dict(email=email, password=password),
            follow_redirects=True,
        )

    def logout(self):
        return self.client.get(url_for("auth.logout"), follow_redirects=True)

    def test_registration_and_login(self):
        data = {
            "email": "john@example.com",
            "firstname": "Jörg",
            "lastname": "Doe",
            "password": "PasswordSuperSafe123!",
            "password2": "PasswordSuperSafe123!",
            "spamprotect": "",
        }
        # register new account
        response = self.client.post(url_for("auth.register"), data=data)
        assert response.status_code == 302

        # login with new account
        response = self.login(data["email"], data["password"])
        text = response.get_data(as_text=True)
        assert "Confirm Account" in text

        # send confirmation token
        user = User.query.filter_by(email=data["email"]).first()
        token = user.generate_confirmation_token()
        response = self.client.get(
            url_for("auth.confirm", token=token), follow_redirects=True
        )
        db.session.commit()
        assert user.confirmed

        # logout
        response = self.logout()
        text = response.get_data(as_text=True)
        assert "You have been logged out." in text

        # login again
        response = self.login(data["email"], data["password"])
        text = response.get_data(as_text=True)
        assert "Membership" in text

    def register_user(self):
        # create member in database
        user = User(
            email=self.user_data["email"],
            firstname=self.user_data["firstname"],
            lastname=self.user_data["lastname"],
            confirmed=True,
        )
        user.password_hash = generate_password_hash(self.user_data["password"])
        db.session.add(user)
        db.session.commit()

    def test_details_my_account(self):
        self.register_user()
        self.login(self.user_data["email"], self.user_data["password"])

        response = self.client.get(
            url_for("account.details_my_account"), follow_redirects=True
        )
        text = response.get_data(as_text=True)
        assert "My Account" in text


# TODO:
# - update account
# - deactivate account
# - change password
# - change email
