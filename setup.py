import io
import setuptools

with io.open("README.md", encoding="utf-8") as f:
    readme = f.read()

setuptools.setup(
    name="clubhub",
    version="1.0.0",
    description="ClubHub",
    long_description=readme,
    long_description_content_type="text/markdown",
    license="GPL",
    packages=setuptools.find_namespace_packages(where="src"),
    package_dir={"": "src"},
    python_requires='>=3.6',
    install_requires=[
        'flask',
        'flask_admin',
        'flask_bootstrap',
        'flask_login',
        'flask_mail',
        'flask_migrate',
        'flask_nav',
        'flask_sslify',
        'flask_sqlalchemy',
        'flask_wtf',
        'email_validator',
        'itsdangerous==2.0.1',
        'python-dotenv',
        'requests',
        'safe',
        'WTForms==2.3.3',
        'werkzeug',
        'pymysql',
        'python-dateutil',
    ],
    include_package_data=True,
)
