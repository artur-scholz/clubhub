from flask_login import current_user
from flask_admin import Admin
from flask_admin.menu import MenuLink
from flask_admin.contrib.sqla import ModelView
from wtforms import PasswordField

from .models import (
    User,
    Club,
    Profile,
    Company,
    SuperAdmin,
    Membership,
    Employment,
    Family,
)


def create_admin_interface(app, db):
    admin = Admin(app, name="Database Admin")
    admin.add_view(UserView(User, db.session, category="Account"))
    admin.add_view(FilterView(Profile, db.session, category="Account"))
    admin.add_view(FilterView(Family, db.session, category="Account"))
    admin.add_view(MyModelView(Club, db.session, endpoint="club_"))
    admin.add_view(FilterView(Membership, db.session, endpoint="membership_"))
    admin.add_view(MyModelView(Company, db.session, endpoint="company_"))
    admin.add_view(FilterView(Employment, db.session))
    admin.add_view(FilterView(SuperAdmin, db.session))
    admin.add_link(MenuLink(name="Exit", url="/", target="/"))


class MyModelView(ModelView):
    create_modal = True
    edit_modal = True
    can_export = True

    # Overwrite the generic model view to require authentication
    def is_accessible(self):
        if current_user.is_authenticated:
            return current_user.is_superadmin()
        return False


class FilterView(MyModelView):
    column_searchable_list = ["user.email", "user.firstname", "user.lastname"]


class UserView(MyModelView):
    column_exclude_list = [
        "password_hash",
    ]
    column_searchable_list = ["email", "firstname", "lastname"]
    form_columns = (
        "email",
        "firstname",
        "lastname",
        "password_new",
        "confirmed",
    )
    form_excluded_columns = "password_hash"
    form_extra_fields = {"password_new": PasswordField("Password")}

    def on_model_change(self, form, User, is_created):
        if form.password_new.data != "":
            User.password = form.password_new.data
