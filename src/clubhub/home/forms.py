from flask_wtf import FlaskForm
from wtforms import TextAreaField, SubmitField, StringField
from wtforms.validators import Length
from wtforms import ValidationError


class FeedbackForm(FlaskForm):
    text = TextAreaField(
        "",
        render_kw={"class": "form-control", "rows": 10},
        validators=[Length(max=4000)],
    )
    spamprotect = StringField(
        "Spam protection", default="Please delete this text"
    )
    submit = SubmitField()

    def validate_spamprotect(self, field):
        if len(field.data) > 0:
            raise ValidationError("You must clear this field.")
