import os

from flask import render_template, redirect, url_for, flash
from flask_login import current_user, login_required

from clubhub.models import Club, Company
from clubhub.email import send_email
from clubhub.utils import parse_arg
from . import home
from .forms import FeedbackForm


@home.route("/")
def index():
    if current_user.is_authenticated:
        my_clubs = current_user.get_clubs()
        pending_memberships = current_user.get_pending_memberships()
        my_clubs_to_admin = current_user.get_clubs_to_admin()
        my_companies_to_admin = current_user.get_companies_to_admin()
    else:
        my_clubs = None
        pending_memberships = None
        my_clubs_to_admin = None
        my_companies_to_admin = None
    context = {
        "my_clubs": my_clubs,
        "pending_memberships": pending_memberships,
        "my_clubs_to_admin": my_clubs_to_admin,
        "my_companies_to_admin": my_companies_to_admin,
    }
    return render_template("home/index.html", context=context)


@home.route("/about")
def about():
    return render_template("home/about.html")


@home.route("/user_guide")
def user_guide():
    return render_template("home/user_guide.html")


@home.route("/privacy_policy")
def privacy_policy():
    return render_template("home/privacy_policy.html")


@home.route("/feedback", methods=["GET", "POST"])
def feedback():
    form = FeedbackForm()
    if form.validate_on_submit():
        user = current_user if current_user.is_authenticated else None
        text = form.text.data
        if send_email(
            os.environ.get("FEEDBACK_EMAIL"),
            "User Feedback",
            "home/email/feedback",
            user=user,
            text=text,
        ):
            flash("Thanks for your feedback!")
        return redirect(url_for("home.feedback"))
    return render_template("home/feedback.html", form=form)


@home.route("/menu-my-club")
@login_required
def menu_my_club():
    club_id = parse_arg("club_id", int)
    club = Club.query.get_or_404(club_id)
    memberships = current_user.get_active_memberships(club)
    context = {
        "club": club,
        "memberships": memberships,
    }
    return render_template("home/menu_my_club.html", context=context)


@home.route("/menu-club-administration")
@login_required
def menu_club_administration():
    club_id = parse_arg("club_id", int)
    club = Club.query.get_or_404(club_id)
    roles = current_user.get_club_roles(club)
    context = {
        "club": club,
        "roles": ", ".join(roles),
    }
    return render_template(
        "home/menu_club_administration.html", context=context
    )


@home.route("/menu-company-administration")
@login_required
def menu_company_administration():
    company_id = parse_arg("company_id", int)
    company = Company.query.get_or_404(company_id)
    roles = current_user.get_company_roles(company)
    context = {
        "company": company,
        "roles": ", ".join(roles),
    }
    return render_template(
        "home/menu_company_administration.html", context=context
    )
