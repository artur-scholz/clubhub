import os

from .core import create_app


app = create_app(os.getenv("CONFIG") or "development")
