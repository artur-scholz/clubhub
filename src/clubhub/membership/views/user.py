from flask import render_template, flash, redirect
from flask_login import current_user, login_required

from clubhub.core import db
from clubhub.models import Membership, Club
from clubhub.email import send_email
from clubhub.utils import (
    parse_arg,
    get_conflicting_memberships,
    is_pending_membership,
)
from .. import membership
from ..forms import RequestMembershipForm


@membership.route("/list-my-memberships-history")
@login_required
def list_my_memberships_history():
    memberships = (
        Membership.query.filter_by(user=current_user, confirmed=True)
        .order_by(Membership.valid_from.desc())
        .all()
    )
    context = {
        "memberships": memberships,
    }
    return render_template(
        "membership/list_my_memberships.html",
        title="Memberships History",
        context=context,
    )


@membership.route("/request-membership", methods=["GET", "POST"])
@login_required
def request_membership():
    club_id = parse_arg("club_id", int)
    club = Club.query.get_or_404(club_id) if club_id else None
    form = RequestMembershipForm()
    choices = []
    choices.extend([(c.id, c.name) for c in Club.get_all()])
    form.club_id.choices = choices

    if form.validate_on_submit():
        if form.club_id.data:
            club = Club.query.get_or_404(form.club_id.data)
            if not club.email:
                flash(
                    "There is no contact email for the {} club. "
                    "Please inform SSCC!".format(club.name)
                )
            else:
                valid_from = form.valid_from.data
                valid_till = form.valid_till.data
                conflicting_memberships = get_conflicting_memberships(
                    current_user.id, club_id, valid_from, valid_till
                )
                if conflicting_memberships:
                    flash(
                        "The requested membership is in conflict with the "
                        "following existing membership(s):"
                    )
                    for conflicting_membership in conflicting_memberships:
                        flash(
                            "Membership from {} till {}.".format(
                                conflicting_membership.valid_from,
                                conflicting_membership.valid_till,
                            )
                        )
                elif is_pending_membership(
                    current_user.id, club_id, valid_from, valid_till
                ):
                    flash(
                        "You already submitted such membership request, "
                        "which is pending confirmation. "
                    )
                else:
                    membership = Membership(
                        user=current_user,
                        club=club,
                        valid_from=valid_from,
                        valid_till=valid_till,
                    )
                    db.session.add(membership)
                    db.session.commit()
                    if send_email(
                        club.email,
                        "Membership Request",
                        "membership/email/request_membership",
                        user=current_user,
                        club=club,
                        valid_from=valid_from,
                        valid_till=valid_till,
                    ):
                        flash(
                            "A membership request email for the {} club has "
                            "been sent.".format(club.name)
                        )
                    return redirect("/")
    else:
        if club:
            form.club_id.data = club.id
    note = (
        "When you request a membership to a club, an email will be sent to "
        "the club management. Your membership becomes valid once they approve "
        "your request. By making this request, you acknowledge the terms and "
        "conditions of the club."
    )
    return render_template(
        "form.html", title="Request Membership", note=note, form=form
    )


# @membership.route('/list-pending-memberships')
# @login_required
# def list_pending_memberships():
#     pending_memberships = current_user.get_pending_memberships()
#     club_name_dict = Club.create_club_name_dict()
#     context = {
#         'memberships': pending_memberships,
#         'club_name_dict': club_name_dict
#     }
#     return render_template('membership/list_my_memberships.html',
#                            title='Pending Membership Requests',
#                            context=context)
