import collections
from datetime import date
import io
import csv

from flask import render_template, make_response
from flask_login import login_required

from clubhub.models import Club, Membership
from clubhub.utils import superadmin_required
from .. import membership
from ..forms import SelectYearForm, SelectDateForm


@membership.route("/list-members-by-year", methods=["GET", "POST"])
@login_required
@superadmin_required
def list_members_by_year():
    form = SelectYearForm()
    choices = [(0, "---")]
    choices.extend([(c.id, c.name) for c in Club.get_all()])
    form.club_id.choices = choices

    if form.validate_on_submit():
        year = form.year.data
        club = Club.query.get(form.club_id.data)
        club_memberships = collections.OrderedDict()
        if club:
            clubs = [club]
        else:
            clubs = Club.get_all()
        for club in clubs:
            memberships = Membership.query.filter_by(club=club, confirmed=True)
            memberships = memberships.filter(
                Membership.valid_from <= date(year, 12, 31),
                Membership.valid_till >= date(year, 1, 1),
            ).all()
            memberships = sorted(
                memberships, key=lambda x: x.user.firstname.lower()
            )
            club_memberships[club] = memberships
        context = {"club_memberships": club_memberships}
        return render_template(
            "membership/list_members_by_club.html", context=context
        )

    return render_template("form.html", form=form)


@membership.route("/list-members-by-date", methods=["GET", "POST"])
@login_required
@superadmin_required
def list_members_by_date():
    form = SelectDateForm()
    choices = [(0, "---")]
    choices.extend([(c.id, c.name) for c in Club.get_all()])
    form.club_id.choices = choices

    if form.validate_on_submit():
        date = form.date.data
        club = Club.query.get(form.club_id.data)
        if club:
            clubs = [club]
        else:
            clubs = Club.get_all()
        club_memberships = collections.OrderedDict()
        for club in clubs:
            memberships = Membership.query.filter_by(club=club, confirmed=True)
            memberships = memberships.filter(
                Membership.valid_from <= date,
                Membership.valid_till >= date,
            ).all()
            memberships = sorted(
                memberships, key=lambda x: x.user.firstname.lower()
            )
            club_memberships[club] = memberships
        context = {"club_memberships": club_memberships}
        return render_template(
            "membership/list_members_by_club.html", context=context
        )

    return render_template("form.html", form=form)


@membership.route("/export-members-per-club", methods=["GET", "POST"])
@login_required
@superadmin_required
def export_members_per_club():
    form = SelectYearForm()
    choices = [(0, "---")]
    choices.extend([(c.id, c.name) for c in Club.get_all()])
    form.club_id.choices = choices

    if form.validate_on_submit():
        year = form.year.data
        club = Club.query.get(form.club_id.data)
        if club:
            clubs = [club]
        else:
            clubs = Club.get_all()
        club_memberships = collections.OrderedDict()
        for club in clubs:
            memberships = Membership.query.filter_by(club=club, confirmed=True)
            memberships = memberships.filter(
                Membership.valid_from <= date(year, 12, 31),
                Membership.valid_till >= date(year, 1, 1),
            ).all()
            memberships = sorted(
                memberships, key=lambda x: x.user.firstname.lower()
            )
            club_memberships[club] = memberships

        si = io.StringIO()
        cw = csv.writer(si)
        cw.writerow(["NAME", "COMPANY", "STATUS"])
        for club, memberships in club_memberships.items():
            cw.writerow("")
            cw.writerow("")
            cw.writerow([club.name])
            cw.writerow("")
            for membership in memberships:
                row = [
                    "{} {}".format(
                        membership.user.firstname, membership.user.lastname
                    ),
                    membership.user.get_company_or_family_relation(),
                    membership.get_status(),
                ]
                cw.writerow(row)
        output = make_response(
            "\uFEFF".encode("utf-8") + si.getvalue().encode("utf-8")
        )
        output.headers[
            "Content-Disposition"
        ] = "attachment; filename=members_per_club.csv"
        output.headers["Content-type"] = "text/csv"
        return output

    return render_template("form.html", form=form)


@membership.route("/export-totals-per-club", methods=["GET", "POST"])
@login_required
@superadmin_required
def export_totals_per_club():
    form = SelectYearForm()
    choices = [(0, "---")]
    choices.extend([(c.id, c.name) for c in Club.get_all()])
    form.club_id.choices = choices

    if form.validate_on_submit():
        year = form.year.data
        club = Club.query.get(form.club_id.data)
        if club:
            clubs = [club]
        else:
            clubs = Club.get_all()
        club_memberships = collections.OrderedDict()
        for club in clubs:
            memberships = Membership.query.filter_by(club=club, confirmed=True)
            memberships = memberships.filter(
                Membership.valid_from <= date(year, 12, 31),
                Membership.valid_till >= date(year, 1, 1),
            ).all()
            memberships = sorted(
                memberships, key=lambda x: x.user.firstname.lower()
            )
            club_memberships[club] = memberships

        si = io.StringIO()
        cw = csv.writer(si)
        cw.writerow(
            [
                "CLUB",
                "Eumetsat Staff & Cont",
                "ESA Staff",
                "ESA Contr Internal",
                "Partners",
                "Externals",
            ]
        )
        cw.writerow("")
        for club, memberships in club_memberships.items():
            esa_staff = 0
            eumetsat = 0
            contractor = 0
            partners = 0
            externals = 0
            for membership in memberships:
                status = membership.get_status()
                company = membership.user.get_company()
                if status == "External":
                    externals += 1
                elif status == "Internal":
                    if membership.user.is_partner():
                        partners += 1
                    elif company.name.upper() == "ESA":
                        esa_staff += 1
                    elif "EUMETSAT" in company.name.upper():
                        eumetsat += 1
                    else:
                        contractor += 1
                row = [
                    club.name,
                    eumetsat,
                    esa_staff,
                    contractor,
                    partners,
                    externals,
                ]
            cw.writerow(row)
        output = make_response(si.getvalue())
        output.headers[
            "Content-Disposition"
        ] = "attachment; filename=totals_per_club.csv"
        output.headers["Content-type"] = "text/csv"
        return output

    return render_template("form.html", form=form)
