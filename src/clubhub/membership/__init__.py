from flask import Blueprint

membership = Blueprint(
    "membership", __name__, template_folder="templates", static_folder="static"
)

from . import views
