from datetime import date

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField, IntegerField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Email, Optional

from clubhub.utils import validate_user_email, validate_time_range


def get_valid_from():
    today = date.today()
    if today.month == 12:
        return date(date.today().year + 1, 1, 1)
    return date(date.today().year, 1, 1)


def get_valid_till():
    today = date.today()
    if today.month == 12:
        return date(date.today().year + 1, 12, 31)
    return date(date.today().year, 12, 31)


class RequestMembershipForm(FlaskForm):
    club_id = SelectField("Club", coerce=int)
    valid_from = DateField(default=get_valid_from)
    valid_till = DateField(default=get_valid_till)
    submit = SubmitField()

    def validate(self):
        if super().validate():
            return validate_time_range(
                self, self.valid_from.data, self.valid_till.data
            )
        return False


class ModifyMembershipForm(FlaskForm):
    user_email = StringField(render_kw={"readonly": True})
    club = StringField(render_kw={"readonly": True})
    valid_from = DateField(validators=[DataRequired()])
    valid_till = DateField(validators=[DataRequired()])
    submit = SubmitField()

    def validate(self):
        if super().validate():
            return validate_time_range(
                self, self.valid_from.data, self.valid_till.data
            )
        return False

    def validate_user_email(self, field):
        validate_user_email(self, field)


class ConfirmMembershipForm(FlaskForm):
    user = StringField(render_kw={"readonly": True})
    club = StringField(render_kw={"readonly": True})
    valid_from = DateField(validators=[DataRequired()])
    valid_till = DateField(validators=[DataRequired()])
    submit = SubmitField()

    def validate(self):
        if super().validate():
            return validate_time_range(
                self, self.valid_from.data, self.valid_till.data
            )
        return False


class CreateMembershipForm(FlaskForm):
    user_email = StringField(validators=[Email()])
    club = StringField(render_kw={"readonly": True})
    valid_from = DateField(default=get_valid_from, validators=[DataRequired()])
    valid_till = DateField(default=get_valid_till, validators=[DataRequired()])
    submit = SubmitField()

    def validate_user_email(self, field):
        validate_user_email(self, field)

    def validate(self):
        if super().validate():
            return validate_time_range(
                self, self.valid_from.data, self.valid_till.data
            )
        return False


class SelectYearForm(FlaskForm):
    year = IntegerField(default=date.today().year, validators=[DataRequired()])
    club_id = SelectField(
        "Club", coerce=int, description="Leave empty for all clubs"
    )
    submit = SubmitField()


class SelectDateForm(FlaskForm):
    date = DateField(default=date.today(), validators=[DataRequired()])
    club_id = SelectField(
        "Club", coerce=int, description="Leave empty for all clubs"
    )
    submit = SubmitField()
