from flask_login import current_user
from flask_nav.elements import Navbar, View, Subgroup, Separator, Text


def top_nav():
    navbar = Navbar(View("ClubHub", "home.index"))

    if current_user.is_authenticated:
        subgroup = Subgroup(current_user.firstname or current_user.email)
        subgroup.items.append(View("My Account", "account.details_my_account"))
        subgroup.items.append(Separator())
        subgroup.items.append(
            View("Update My Account", "account.update_my_account")
        )
        subgroup.items.append(
            View("Deactivate My Account", "account.deactivate_my_account")
        )
        subgroup.items.append(Separator())
        subgroup.items.append(View("Change Password", "auth.change_password"))
        subgroup.items.append(
            View("Change Email", "auth.change_email_request")
        )
        subgroup.items.append(Separator())
        subgroup.items.append(View("Logout", "auth.logout"))
        navbar.items.append(subgroup)

        # subgroup = Subgroup('Membership')
        # navbar.items.append(subgroup)
        # subgroup.items.append(
        #     View('Active Memberships',
        #          'membership.list_my_active_memberships'))
        # subgroup.items.append(
        #     View('Memberships History',
        #          'membership.list_my_memberships_history'))
        # subgroup.items.append(Separator())
        # subgroup.items.append(
        #     View('Request Membership', 'membership.request_membership'))
        # subgroup.items.append(
        #     View('Pending Requests', 'membership.list_pending_memberships'))

        navbar.items.append(View("Clubs", "club.list_all_clubs"))
        navbar.items.append(View("Companies", "company.list_all_companies"))

        if current_user.is_superadmin():
            subgroup = Subgroup("SuperAdmin")
            navbar.items.append(subgroup)
            subgroup.items.append(View("DataBase", "admin.index"))
            subgroup.items.append(Separator())
            subgroup.items.append(
                View("List Confirmed Users", "account.list_confirmed_users")
            )
            subgroup.items.append(
                View(
                    "List Unconfirmed Users", "account.list_unconfirmed_users"
                )
            )
            subgroup.items.append(Separator())
            subgroup.items.append(
                View("Create Company", "company.create_company")
            )
            subgroup.items.append(
                View("Update Company", "company.select_company_to_update")
            )
            subgroup.items.append(
                View("Delete Company", "company.delete_company")
            )
            subgroup.items.append(Separator())
            subgroup.items.append(View("Create Club", "club.create_club"))
            subgroup.items.append(
                View("Update Club", "club.select_club_to_update")
            )
            subgroup.items.append(View("Delete Club", "club.delete_club"))
            subgroup.items.append(Separator())
            subgroup.items.append(Text("Reports"))
            subgroup.items.append(
                View("List Members by Year", "membership.list_members_by_year")
            )
            subgroup.items.append(
                View("List Members by Date", "membership.list_members_by_date")
            )
            subgroup.items.append(
                View("List Users by Company", "company.list_users_by_company")
            )
            subgroup.items.append(Separator())
            subgroup.items.append(Text("Export"))
            subgroup.items.append(
                View(
                    "Members per Club by Year",
                    "membership.export_members_per_club",
                )
            )
            subgroup.items.append(
                View(
                    "Totals per Club by Year",
                    "membership.export_totals_per_club",
                )
            )
    else:
        navbar.items.append(View("Login", "auth.login"))
        navbar.items.append(View("Register", "auth.register"))

    subgroup = Subgroup("Help")
    navbar.items.append(subgroup)
    subgroup.items.append(View("User Guide", "home.user_guide"))
    subgroup.items.append(View("Privacy Policy", "home.privacy_policy"))
    subgroup.items.append(View("Feedback", "home.feedback"))
    subgroup.items.append(View("About", "home.about"))

    return navbar
