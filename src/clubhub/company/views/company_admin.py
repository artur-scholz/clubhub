import collections

from flask import render_template, flash, redirect, url_for
from flask_login import current_user, login_required

from clubhub.core import db
from clubhub.models import User, Company
from clubhub.utils import parse_arg, require_company_admin
from .. import company
from ..forms import UpdateCompanyForm


@company.route("/update-company", methods=["GET", "POST"])
@login_required
def update_company():
    company_id = parse_arg("company_id", int)
    company = Company.query.get_or_404(company_id)
    require_company_admin(current_user, company)

    form = UpdateCompanyForm(obj=company)
    if form.validate_on_submit():
        name = form.name.data
        contributing = form.contributing.data
        address = form.address.data if form.address.data != "" else None
        email = form.email.data if form.email.data != "" else None
        website = form.website.data if form.website.data != "" else None
        officer = User.query.filter_by(email=form.officer_email.data).first()
        deputy = User.query.filter_by(email=form.deputy_email.data).first()
        company.name = name
        company.contributing = contributing
        company.address = address
        company.email = email
        company.website = website
        company.officer = officer if officer else None
        company.deputy = deputy if deputy else None
        db.session.add(company)
        flash("Company {} updated.".format(company.name))
        return redirect(url_for(".update_company", company_id=company.id))
    else:
        if company.officer:
            form.officer_email.data = company.officer.email
        if company.deputy:
            form.deputy_email.data = company.deputy.email
    return render_template("form.html", title="Update Company", form=form)


@company.route("/list-employees")
@login_required
def list_employees():
    company_id = parse_arg("company_id", int)
    company = Company.query.get_or_404(company_id)
    require_company_admin(current_user, company)
    company_employees = collections.OrderedDict()
    employee_partner = collections.OrderedDict()
    employee_children = collections.OrderedDict()
    employees = []
    for employment in company.employments:
        user = employment.user
        employees.append(user)
        employee_partner[user] = user.get_right_partner()
        employee_children[user] = user.get_children()
    company_employees[company] = sorted(
        employees, key=lambda x: x.firstname.lower()
    )
    context = {
        "company_employees": company_employees,
        "employee_partner": employee_partner,
        "employee_children": employee_children,
    }
    return render_template(
        "company/list_users_by_company.html", context=context
    )
