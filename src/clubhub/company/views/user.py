from flask import render_template
from flask_login import login_required

from clubhub.models import Company
from clubhub.utils import parse_arg
from .. import company


@company.route("/list-all-companies")
@login_required
def list_all_companies():
    companies = Company.get_all()
    context = {"companies": companies}
    return render_template(
        "company/list_companies.html", title="All Companies", context=context
    )


@company.route("/details-company")
@login_required
def details_company():
    company_id = parse_arg("company_id", int)
    company = Company.query.get_or_404(company_id)
    context = {
        "company": company,
        "officer": company.officer,
        "deputy": company.deputy,
    }
    return render_template("company/details_company.html", context=context)
