import collections

from flask import render_template, flash, redirect, url_for
from flask_login import login_required

from clubhub.core import db
from clubhub.models import User, Company
from clubhub.utils import superadmin_required
from .. import company
from ..forms import CreateCompanyForm, DeleteCompanyForm, SelectCompanyForm


@company.route("/create-company", methods=["GET", "POST"])
@login_required
@superadmin_required
def create_company():
    form = CreateCompanyForm()
    if form.validate_on_submit():
        name = form.name.data
        contributing = form.contributing.data
        address = form.address.data if form.address.data != "" else None
        email = form.email.data if form.email.data != "" else None
        website = form.website.data if form.website.data != "" else None
        officer = User.query.filter_by(email=form.officer_email.data).first()
        deputy = User.query.filter_by(email=form.deputy_email.data).first()
        company = Company(
            name=name,
            contributing=contributing,
            address=address,
            email=email,
            website=website,
            officer=officer if officer else None,
            deputy=deputy if deputy else None,
        )
        db.session.add(company)
        db.session.commit()
        flash("{} created.".format(company.name))
        return redirect(url_for(".list_all_companies"))
    return render_template("form.html", title="Create Company", form=form)


@company.route("/select-company-to-update", methods=["GET", "POST"])
@login_required
@superadmin_required
def select_company_to_update():
    form = SelectCompanyForm()
    form.company_id.choices = [(c.id, c.name) for c in Company.get_all()]
    if form.validate_on_submit():
        company_id = form.company_id.data
        return redirect(
            url_for("company.update_company", company_id=company_id)
        )
    return render_template(
        "form.html", title="Select Company to Update", form=form
    )


@company.route("/delete-company", methods=["GET", "POST"])
@login_required
@superadmin_required
def delete_company():
    form = DeleteCompanyForm()
    form.company_id.choices = [(c.id, c.name) for c in Company.get_all()]
    if form.validate_on_submit():
        company = Company.query.get(form.company_id.data)
        db.session.delete(company)
        db.session.commit()
        flash("Company {} deleted.".format(company.name))
        return redirect(url_for(".delete_company"))
    return render_template("form.html", title="Delete Company", form=form)


@company.route("/list-users-by-company")
@login_required
@superadmin_required
def list_users_by_company():
    companies = Company.query.order_by(Company.name).all()
    company_employees = collections.OrderedDict()
    employee_partner = collections.OrderedDict()
    employee_children = collections.OrderedDict()
    for company in companies:
        employees = []
        for employment in company.employments:
            user = employment.user
            employees.append(user)
            employee_partner[user] = user.get_right_partner()
            employee_children[user] = user.get_children()
        company_employees[company] = sorted(
            employees, key=lambda x: x.firstname.lower()
        )
    context = {
        "company_employees": company_employees,
        "employee_partner": employee_partner,
        "employee_children": employee_children,
    }
    return render_template(
        "company/list_users_by_company.html", context=context
    )
