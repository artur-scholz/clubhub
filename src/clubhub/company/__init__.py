from flask import Blueprint

company = Blueprint(
    "company", __name__, template_folder="templates", static_folder="static"
)

from . import views
