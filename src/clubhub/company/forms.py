from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    SubmitField,
    TextAreaField,
    SelectField,
    BooleanField,
)
from wtforms.validators import DataRequired, Length, Email, Optional, URL
from wtforms import ValidationError

from clubhub.models import User, Company


def validate_user_email(self, field):
    field.data = field.data.lower()
    if not User.query.filter_by(email=field.data).first():
        raise ValidationError("No user with that email found.")


class CreateCompanyForm(FlaskForm):
    name = StringField(validators=[DataRequired(), Length(1, 64)])
    contributing = BooleanField("Contributing")
    address = TextAreaField(validators=[Optional()])
    email = StringField(validators=[Email(), Optional()])
    website = StringField(validators=[Length(0, 256), URL(), Optional()])
    officer_email = StringField(validators=[Email(), Optional()])
    deputy_email = StringField(validators=[Email(), Optional()])
    submit = SubmitField()

    def validate_name(self, field):
        if Company.query.filter_by(name=field.data).first():
            raise ValidationError("Company exists already.")

    def validate_officer_email(self, field):
        validate_user_email(self, field)

    def validate_deputy_email(self, field):
        validate_user_email(self, field)


class SelectCompanyForm(FlaskForm):
    company_id = SelectField("Company", coerce=int)
    submit = SubmitField()


class UpdateCompanyForm(FlaskForm):
    name = StringField(validators=[DataRequired(), Length(1, 64)])
    contributing = BooleanField("Contributing")
    address = TextAreaField(validators=[Optional()])
    email = StringField(validators=[Email(), Optional()])
    website = StringField(validators=[Length(0, 256), URL(), Optional()])
    officer_email = StringField(validators=[Email(), Optional()])
    deputy_email = StringField(validators=[Email(), Optional()])
    submit = SubmitField()

    def validate_officer_email(self, field):
        validate_user_email(self, field)

    def validate_deputy_email(self, field):
        validate_user_email(self, field)


class DeleteCompanyForm(FlaskForm):
    company_id = SelectField("Company", coerce=int)
    confirm_name = StringField(validators=[DataRequired()])
    submit = SubmitField("Delete")

    def validate(self):
        if super().validate():
            company = Company.query.get_or_404(self.company_id.data)
            if company.name == self.confirm_name.data:
                return True
            else:
                self.confirm_name.errors.append("Name does not match.")
                return False
        return False
