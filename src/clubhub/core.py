import os
import sys

if sys.version_info.major == 3 and sys.version_info.minor >= 10:
    import collections

    setattr(collections, "MutableMapping", collections.abc.MutableMapping)

from flask import Flask, redirect, url_for
from flask_sslify import SSLify
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_mail import Mail
from flask_bootstrap import Bootstrap
from flask_nav import Nav

from .config import config


db = SQLAlchemy()
migrate = Migrate()
login_manager = LoginManager()
mail = Mail()
bootstrap = Bootstrap()
nav = Nav()

login_manager.session_protection = "strong"
login_manager.login_view = "auth.login"


def create_app(config_name):

    app = Flask(__name__)
    app.config.from_object(config[config_name])

    if int(os.environ.get("FORCE_HTTPS", 0)):
        SSLify(app)  # redirect http to https

    db.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)
    mail.init_app(app)
    bootstrap.init_app(app)
    app.config["BOOTSTRAP_SERVE_LOCAL"] = True

    from .http import register_error_handlers

    register_error_handlers(app)

    from .admin import create_admin_interface

    create_admin_interface(app, db)

    from .navbar import top_nav

    nav.init_app(app)
    nav.register_element("top_nav", top_nav)

    from clubhub.home import home
    from clubhub.auth import auth
    from clubhub.account import account
    from clubhub.club import club
    from clubhub.company import company
    from clubhub.membership import membership

    app.register_blueprint(home, url_prefix="/home")
    app.register_blueprint(auth, url_prefix="/auth")
    app.register_blueprint(account, url_prefix="/account")
    app.register_blueprint(club, url_prefix="/club")
    app.register_blueprint(company, url_prefix="/company")
    app.register_blueprint(membership, url_prefix="/membership")

    @app.route("/")
    def index():
        return redirect(url_for("home.index"))

    return app
