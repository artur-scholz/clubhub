import hashlib
import datetime

from .core import db
from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app
from flask_login import UserMixin


class Profile(db.Model):
    """A profile is linked to a user and contains additional, non-mandatory
    information about the user."""

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer, db.ForeignKey("user.id"), nullable=False, unique=True
    )
    user = db.relationship("User", back_populates="profile")
    phone = db.Column(db.String(32))
    about = db.Column(db.Text)

    def __repr__(self):
        return "<Profile ({})>".format(self.user)


class SuperAdmin(db.Model):
    """The superadmin group has full control of the website content."""

    __tablename__ = "superadmin"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), unique=True)
    user = db.relationship("User", back_populates="superadmin")

    def __repr__(self):
        return "<SuperAdmin ({})>".format(self.user)


class Club(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True, nullable=False)
    description = db.Column(db.Text)
    email = db.Column(db.String(256))
    website = db.Column(db.String(256))
    chairman_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    chairman = db.relationship("User", foreign_keys=chairman_id)
    secretary_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    secretary = db.relationship("User", foreign_keys=secretary_id)
    treasurer_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    treasurer = db.relationship("User", foreign_keys=treasurer_id)
    inventory_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    inventory = db.relationship("User", foreign_keys=inventory_id)

    def __repr__(self):
        return "<Club ({})>".format(self.name)

    @classmethod
    def get_all(cls):
        return cls.query.order_by(cls.name).all()

    def get_active_memberships(self, date=datetime.date.today()):
        memberships = Membership.query.filter_by(club=self, confirmed=True)
        memberships = memberships.filter(
            Membership.valid_from <= date, Membership.valid_till >= date
        ).all()
        return memberships

    def get_membership_requests(self):
        users = []
        requests = []
        unconfirmed_memberships = Membership.query.filter_by(
            club=self, confirmed=False
        ).all()
        for unconfirmed_membership in unconfirmed_memberships:
            user = User.query.get(unconfirmed_membership.user_id)
            users.append(user)
            requests.append(unconfirmed_membership)
        return users, requests

    @classmethod
    def create_club_name_dict(cls):
        # create a dictionary that maps club ids to club names
        club_name_dict = {}
        for club in cls.get_all():
            club_name_dict[club.id] = club.name
        return club_name_dict


class Membership(db.Model):
    """A membership is a contract between user and club and has a defined start
    and end time."""

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    user = db.relationship("User", uselist=False, backref="memberships")
    club_id = db.Column(db.Integer, db.ForeignKey("club.id"), nullable=False)
    club = db.relationship("Club", uselist=False, backref="memberships")
    valid_from = db.Column(db.Date, nullable=False)
    valid_till = db.Column(db.Date, nullable=False)
    confirmed = db.Column(db.Boolean, default=False)

    def confirm(self, token):
        self.confirmed = True
        db.session.add(self)
        db.session.commit()
        return True

    def __repr__(self):
        return "<Membership ({}, {}, from {} to {})>".format(
            self.user, self.club, self.valid_from, self.valid_till
        )

    def is_valid(self, date=datetime.date.today()):
        # check if the membership is valid for the specified date
        if date >= self.valid_from and date <= self.valid_till:
            return True
        else:
            return False

    def get_status(self):
        if self.user.is_family_member():
            if self.user.is_child():
                return "External"
            elif self.user.is_partner():
                partner = self.user.get_left_partner()
                employment = partner.get_employment()
        else:
            employment = self.user.get_employment()
        if employment:
            if employment.company.contributing:
                return "Internal"
            elif "affiliate" in employment.company.name.lower():
                return "Affiliate"
            else:
                return "External"
        else:
            return "???"


class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True, nullable=False)
    contributing = db.Column(db.Boolean, default=True)
    email = db.Column(db.String(256))
    website = db.Column(db.String(256))
    address = db.Column(db.Text)
    officer_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    officer = db.relationship("User", foreign_keys=officer_id)
    deputy_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    deputy = db.relationship("User", foreign_keys=deputy_id)

    def __repr__(self):
        return "<Company ({})>".format(self.name)

    @classmethod
    def get_all(cls):
        return cls.query.order_by(cls.name).all()


class Employment(db.Model):
    """An employment connects a user and a company. It has no start and end,
    and only one employment is allowed at any instance of time."""

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    user = db.relationship("User", uselist=False, backref="employments")
    company_id = db.Column(
        db.Integer, db.ForeignKey("company.id"), nullable=False
    )
    company = db.relationship("Company", uselist=False, backref="employments")

    __table_args__ = (
        db.UniqueConstraint(
            "user_id", "company_id", name="unique_user_company"
        ),
    )

    def __repr__(self):
        return "<Employment ({}, {})>".format(
            self.user,
            self.company,
        )


class Family(db.Model):
    """Tracks the family relationship (partner, child) among users."""

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    user = db.relationship("User", foreign_keys=user_id)
    other_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    other = db.relationship("User", foreign_keys=other_id)
    is_partner = db.Column(db.Boolean, default=False)
    is_child = db.Column(db.Boolean, default=False)

    __table_args__ = (
        db.UniqueConstraint("user_id", "other_id", name="unique_user_other"),
    )

    def __repr__(self):
        if self.is_partner:
            return "<Family ({}, {} <- partner)>".format(self.user, self.other)
        elif self.is_child:
            return "<Family ({}, {} <- child)>".format(self.user, self.other)


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True, nullable=False)
    firstname = db.Column(db.String(64), index=True)
    lastname = db.Column(db.String(64), index=True)
    password_hash = db.Column(db.String(128))
    confirmed = db.Column(db.Boolean, default=False)

    profile = db.relationship("Profile", uselist=False, back_populates="user")
    superadmin = db.relationship(
        "SuperAdmin", uselist=False, back_populates="user"
    )

    def is_superadmin(self):
        return self.superadmin is not None

    def is_employee(self, company=None):
        if company:
            employment = Employment.query.filter_by(
                user=self, company=company
            ).all()
        else:
            employment = Employment.query.filter_by(user=self).all()
        return True if employment else False

    def is_family_member(self):
        family_member = Family.query.filter_by(other=self).all()
        return True if family_member else False

    def is_club_admin(self, club=None):
        if club:
            if self in [
                club.chairman,
                club.secretary,
                club.treasurer,
                club.inventory,
            ]:
                return True
        else:
            clubs = Club.query.all()
            for club in clubs:
                if self.is_club_admin(club):
                    return True
        return False

    def is_admin_of_user(self, user):
        return self.is_club_admin_of_user(
            user
        ) or self.is_company_admin_of_user(user)

    def is_company_admin(self, company=None):
        if company:
            if self in [company.officer, company.deputy]:
                return True
        else:
            companies = Company.query.all()
            for company in companies:
                if self.is_company_admin(company):
                    return True
        return False

    def get_children(self):
        children = []
        for entry in Family.query.filter_by(user=self, is_child=True).all():
            children.append(entry.other)
        return children

    def get_parent(self):
        entry = Family.query.filter_by(other=self, is_child=True).first()
        return entry.user if entry else None

    def is_child(self):
        return True if self.get_parent() else False

    def get_left_partner(self):
        entry = Family.query.filter_by(other=self, is_partner=True).first()
        return entry.user if entry else None

    def get_right_partner(self):
        entry = Family.query.filter_by(user=self, is_partner=True).first()
        return entry.other if entry else None

    def is_partner(self):
        return True if self.get_left_partner() else False

    def get_profile(self):
        # load profile if exists, otherwise create one
        profile = Profile.query.filter_by(user=self).first()
        if not profile:
            profile = Profile()
            profile.user = self
            db.session.add(profile)
            db.session.commit()
        return profile

    def get_gravatar(self):
        return hashlib.md5(self.email.encode("utf-8")).hexdigest()

    def get_employment(self):
        return Employment.query.filter_by(user=self).first()

    def get_company(self):
        employment = self.get_employment()
        return Company.query.get(employment.company_id) if employment else None

    def get_company_or_family_relation(self):
        company = self.get_company()
        if company:
            return company.name
        partner = self.get_left_partner()
        if partner:
            return "Partner (of {} {})".format(
                partner.firstname, partner.lastname
            )
        parent = self.get_parent()
        if parent:
            return "Child (of {} {})".format(parent.firstname, parent.lastname)
        else:
            return "???"

    def get_clubs(self):
        clubs = []
        for active_membership in self.get_active_memberships():
            clubs.append(Club.query.get(active_membership.club_id))
        return clubs

    def get_clubs_to_admin(self):
        clubs = []
        for club in Club.get_all():
            if self.is_club_admin(club):
                clubs.append(club)
        return clubs

    def get_companies_to_admin(self):
        companies = []
        for company in Company.get_all():
            if self.is_company_admin(company):
                companies.append(company)
        return companies

    def get_club_roles(self, club):
        roles = []
        if self == club.chairman:
            roles.append("Chairman")
        if self == club.secretary:
            roles.append("Secretary")
        if self == club.treasurer:
            roles.append("Treasurer")
        if self == club.inventory:
            roles.append("Inventory")
        return roles

    def is_club_admin_of_user(self, user):
        clubs = []
        for club in Club.get_all():
            if self.is_club_admin(club):
                clubs.append(club)
        for club in clubs:
            if user.is_current_member(club) or user.is_requesting_membership(
                club
            ):
                return True
        return False

    def get_company_roles(self, company):
        roles = []
        if self == company.officer:
            roles.append("Officer")
        if self == company.deputy:
            roles.append("Deputy")
        return roles

    def is_company_admin_of_user(self, user):
        companies = []
        for company in Company.query.all():
            if self.is_company_admin(company):
                companies.append(company)
        for company in companies:
            if user.is_employee(company):
                return True
        return False

    def get_active_memberships(user, club=None):
        if club:
            memberships = Membership.query.filter_by(
                user=user, club=club, confirmed=True
            ).all()
        else:
            memberships = Membership.query.filter_by(
                user=user, confirmed=True
            ).all()
        active_memberships = []
        for membership in memberships:
            if membership.is_valid():
                active_memberships.append(membership)
        return active_memberships

    def get_pending_memberships(self, club=None):
        if club:
            pending_memberships = Membership.query.filter_by(
                user=self, club=club, confirmed=False
            ).all()
        else:
            pending_memberships = Membership.query.filter_by(
                user=self, confirmed=False
            ).all()
        return pending_memberships

    def is_current_member(self, club):
        memberships = Membership.query.filter_by(
            user=self, club=club, confirmed=True
        ).all()
        for membership in memberships:
            if membership.is_valid():
                return True
        return False

    def is_requesting_membership(self, club):
        memberships = Membership.query.filter_by(
            user=self, club=club, confirmed=False
        ).all()
        if memberships:
            return True
        return False

    @property
    def password(self):
        raise AttributeError("password is not a readable attribute")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_confirmation_token(self, expiration=3600):
        s = Serializer(current_app.config["SECRET_KEY"], expiration)
        return s.dumps({"confirm": self.id})

    def confirm(self, token):
        s = Serializer(current_app.config["SECRET_KEY"])
        try:
            data = s.loads(token)
        except Exception:
            return False
        if data.get("confirm") != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        return True

    def generate_reset_token(self, expiration=3600):
        s = Serializer(current_app.config["SECRET_KEY"], expiration)
        return s.dumps({"reset": self.id})

    def reset_password(self, token, new_password):
        s = Serializer(current_app.config["SECRET_KEY"])
        try:
            data = s.loads(token)
        except Exception:
            return False
        if data.get("reset") != self.id:
            return False
        self.password = new_password
        db.session.add(self)
        return True

    def generate_email_change_token(self, new_email, expiration=3600):
        s = Serializer(current_app.config["SECRET_KEY"], expiration)
        return s.dumps({"change_email": self.id, "new_email": new_email})

    def change_email(self, token):
        s = Serializer(current_app.config["SECRET_KEY"])
        try:
            data = s.loads(token)
        except Exception:
            return False
        if data.get("change_email") != self.id:
            return False
        new_email = data.get("new_email")
        if new_email is None:
            return False
        if self.query.filter_by(email=new_email).first() is not None:
            return False
        self.email = new_email
        db.session.add(self)
        return True

    def __repr__(self):
        return "<User ({} {})>".format(self.firstname, self.lastname)
