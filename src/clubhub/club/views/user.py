from flask import render_template
from flask_login import login_required

from clubhub.models import Club
from clubhub.utils import parse_arg
from .. import club


@club.route("/list-all-clubs")
@login_required
def list_all_clubs():
    clubs = Club.get_all()
    context = {"clubs": clubs}
    return render_template(
        "club/list_clubs.html", title="All Clubs", context=context
    )


@club.route("/details-club")
@login_required
def details_club():
    club_id = parse_arg("club_id", int)
    club = Club.query.get_or_404(club_id)
    context = {
        "club": club,
        "chairman": club.chairman,
        "secretary": club.secretary,
        "treasurer": club.treasurer,
        "inventory": club.inventory,
    }
    return render_template("club/details_club.html", context=context)
