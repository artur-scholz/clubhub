from flask import render_template, flash, redirect, url_for
from flask_login import current_user, login_required

from clubhub.core import db
from clubhub.models import User, Club
from clubhub.utils import parse_arg, require_club_admin
from .. import club
from ..forms import UpdateClubForm


@club.route("/update-club", methods=["GET", "POST"])
@login_required
def update_club():
    club_id = parse_arg("club_id", int)
    club = Club.query.get_or_404(club_id)
    require_club_admin(current_user, club)

    form = UpdateClubForm(obj=club)
    if form.validate_on_submit():
        name = form.name.data
        description = (
            form.description.data if form.description.data != "" else None
        )
        email = form.email.data if form.email.data != "" else None
        website = form.website.data if form.website.data != "" else None
        chairman = User.query.filter_by(email=form.chairman_email.data).first()
        secretary = User.query.filter_by(
            email=form.secretary_email.data
        ).first()
        treasurer = User.query.filter_by(
            email=form.treasurer_email.data
        ).first()
        inventory = User.query.filter_by(
            email=form.inventory_email.data
        ).first()
        club.name = name
        club.description = description
        club.email = email
        club.website = website
        club.chairman = chairman if chairman else None
        club.secretary = secretary if secretary else None
        club.treasurer = treasurer if treasurer else None
        club.inventory = inventory if inventory else None
        db.session.add(club)
        db.session.commit()
        flash("{} Club updated.".format(club.name))
        return redirect(url_for(".update_club", club_id=club.id))
    else:
        if club.chairman:
            form.chairman_email.data = club.chairman.email
        if club.secretary:
            form.secretary_email.data = club.secretary.email
        if club.treasurer:
            form.treasurer_email.data = club.treasurer.email
        if club.inventory:
            form.inventory_email.data = club.inventory.email
    return render_template("form.html", title="Update Club", form=form)
