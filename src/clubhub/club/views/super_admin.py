from flask import render_template, flash, redirect, url_for
from flask_login import login_required

from clubhub.core import db
from clubhub.models import User, Club
from clubhub.utils import superadmin_required
from .. import club
from ..forms import CreateClubForm, DeleteClubForm, SelectClubForm


@club.route("/create-club", methods=["GET", "POST"])
@login_required
@superadmin_required
def create_club():
    form = CreateClubForm()
    if form.validate_on_submit():
        name = form.name.data
        description = (
            form.description.data if form.description.data != "" else None
        )
        email = form.email.data if form.email.data != "" else None
        website = form.website.data if form.website.data != "" else None
        chairman = User.query.filter_by(email=form.chairman_email.data).first()
        secretary = User.query.filter_by(
            email=form.secretary_email.data
        ).first()
        treasurer = User.query.filter_by(
            email=form.treasurer_email.data
        ).first()
        inventory = User.query.filter_by(
            email=form.inventory_email.data
        ).first()
        club = Club(
            name=name,
            description=description,
            email=email,
            website=website,
            chairman=chairman if chairman else None,
            secretary=secretary if secretary else None,
            treasurer=treasurer if treasurer else None,
            inventory=inventory if inventory else None,
        )
        db.session.add(club)
        db.session.commit()
        flash("{} Club created.".format(club.name))
        return redirect(url_for("club.list_all_clubs"))
    return render_template("form.html", title="Create Club", form=form)


@club.route("/select-club-to-update", methods=["GET", "POST"])
@login_required
@superadmin_required
def select_club_to_update():
    form = SelectClubForm()
    form.club_id.choices = [(c.id, c.name) for c in Club.get_all()]
    if form.validate_on_submit():
        club_id = form.club_id.data
        return redirect(url_for("club.update_club", club_id=club_id))
    return render_template(
        "form.html", title="Select Club to Update", form=form
    )


@club.route("/delete-club", methods=["GET", "POST"])
@login_required
@superadmin_required
def delete_club():
    form = DeleteClubForm()
    form.club_id.choices = [(c.id, c.name) for c in Club.get_all()]
    if form.validate_on_submit():
        club = Club.query.get(form.club_id.data)
        db.session.delete(club)
        db.session.commit()
        flash("{} Club deleted.".format(club.name))
        return redirect(url_for("club.delete_club"))
    return render_template("form.html", title="Delete Club", form=form)
