from flask import Blueprint

club = Blueprint(
    "club", __name__, template_folder="templates", static_folder="static"
)

from . import views
