from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, SelectField
from wtforms.validators import DataRequired, Length, Email, Optional, URL
from wtforms import ValidationError

from clubhub.models import Club
from clubhub.utils import validate_user_email


class CreateClubForm(FlaskForm):
    name = StringField(validators=[DataRequired(), Length(1, 64)])
    description = TextAreaField(validators=[Optional(), Length(max=4000)])
    email = StringField(validators=[Email(), Optional()])
    website = StringField(validators=[Length(0, 256), URL(), Optional()])
    chairman_email = StringField(validators=[Email(), Optional()])
    secretary_email = StringField(validators=[Email(), Optional()])
    treasurer_email = StringField(validators=[Email(), Optional()])
    inventory_email = StringField(validators=[Email(), Optional()])
    submit = SubmitField()

    def validate_name(self, field):
        if Club.query.filter_by(name=field.data).first():
            raise ValidationError("Club exists already.")

    def validate_chairman_email(self, field):
        validate_user_email(self, field)

    def validate_secretary_email(self, field):
        validate_user_email(self, field)

    def validate_treasurer_email(self, field):
        validate_user_email(self, field)

    def validate_inventory_email(self, field):
        validate_user_email(self, field)


class SelectClubForm(FlaskForm):
    club_id = SelectField("Club", coerce=int)
    submit = SubmitField()


class UpdateClubForm(FlaskForm):
    name = StringField(validators=[DataRequired(), Length(1, 64)])
    description = TextAreaField(
        validators=[Optional(), Length(max=4000)],
        description="(HTML syntax can be used)",
    )
    email = StringField(validators=[Email(), Optional()])
    website = StringField(validators=[Length(0, 256), URL(), Optional()])
    chairman_email = StringField(validators=[Email(), Optional()])
    secretary_email = StringField(validators=[Email(), Optional()])
    treasurer_email = StringField(validators=[Email(), Optional()])
    inventory_email = StringField(validators=[Email(), Optional()])
    submit = SubmitField()

    def validate_chairman_email(self, field):
        validate_user_email(self, field)

    def validate_secretary_email(self, field):
        validate_user_email(self, field)

    def validate_treasurer_email(self, field):
        validate_user_email(self, field)

    def validate_inventory_email(self, field):
        validate_user_email(self, field)


class DeleteClubForm(FlaskForm):
    club_id = SelectField("Club", coerce=int)
    confirm_name = StringField(validators=[DataRequired()])
    submit = SubmitField("Delete")

    def validate(self):
        if super().validate():
            club = Club.query.get_or_404(self.club_id.data)
            if club.name == self.confirm_name.data:
                return True
            else:
                self.confirm_name.errors.append("Name does not match.")
                return False
        return False
