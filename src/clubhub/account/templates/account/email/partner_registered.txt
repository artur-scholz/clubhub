Hello {{ employee.firstname }} {{ employee.lastname }},

{{ partner.firstname }} {{ partner.lastname }} ({{ partner.email }}) has registered as your partner.
