from flask import render_template
from flask_login import current_user, login_required

from clubhub.utils import parse_arg, require_user_admin
from clubhub.models import User
from .. import account


@account.route("/details-user-account")
@login_required
def details_user_account():
    user_id = parse_arg("user_id", int)
    user = User.query.get_or_404(user_id)
    require_user_admin(current_user, user)
    profile = user.get_profile()
    company = user.get_company()
    partner = user.get_left_partner()
    parent = user.get_parent()
    gravatar = user.get_gravatar()

    context = {
        "user": user,
        "profile": profile,
        "company": company,
        "partner": partner,
        "parent": parent,
        "gravatar": gravatar,
    }
    return render_template(
        "account/details_account.html", title="User Account", context=context
    )
