import os

from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_required, logout_user

from clubhub.core import db
from clubhub.email import send_email
from clubhub.models import User, Family, Employment, Company
from .. import account
from ..forms import UpdateAccountForm, DeactivateAccountRequestForm


@account.before_app_request
def before_request():
    if (
        request.url_rule
        and "membership.request_membership" in request.url_rule.endpoint
        and current_user.is_authenticated
        and current_user.confirmed
        and not current_user.is_employee()
        and not current_user.is_family_member()
    ):
        flash("Please update your account with one of the three options.")
        return redirect(url_for("account.update_my_account"))


@account.route("/details-my-account")
@login_required
def details_my_account():
    user = User.query.get_or_404(current_user.id)
    profile = user.get_profile()
    company = user.get_company()
    partner = user.get_left_partner()
    parent = user.get_parent()
    gravatar = user.get_gravatar()

    context = {
        "user": user,
        "profile": profile,
        "company": company,
        "partner": partner,
        "parent": parent,
        "gravatar": gravatar,
    }
    return render_template(
        "account/details_account.html", title="My Account", context=context
    )


@account.route("/update-my-account", methods=["GET", "POST"])
@login_required
def update_my_account():
    user = current_user
    employment = user.get_employment()
    company = user.get_company()
    profile = user.get_profile()
    form = UpdateAccountForm(obj=profile)
    choices = [(0, "---")]
    choices.extend([(c.id, c.name) for c in Company.get_all()])
    form.company_id.choices = choices
    if form.validate_on_submit():
        user.firstname = form.firstname.data
        user.lastname = form.lastname.data
        db.session.add(user)
        profile.phone = form.phone.data
        profile.about = form.about.data
        db.session.add(profile)

        if form.company_id.data:
            # user works for company, hence delete any existing other relation
            Family.query.filter_by(other=user).delete()
            # check if this is a change of company or first definition
            if employment:
                employment.company_id = form.company_id.data
            else:
                employment = Employment(
                    user=user, company_id=form.company_id.data
                )
            db.session.add(employment)
            db.session.commit()
        else:
            # family members do not have an employment entry
            if employment:
                db.session.delete(employment)
                db.session.commit()

            if form.partner_email.data:
                # user is partner, delete is_child record if any
                Family.query.filter_by(other=user, is_child=True).delete()
                left_user = User.query.filter_by(
                    email=form.partner_email.data
                ).first()
                # check if record exists
                entry = Family.query.filter_by(
                    user=left_user, other=user, is_partner=True
                ).first()
                # if record does not exist, create it
                if not entry:
                    entry = Family(user=left_user, other=user, is_partner=True)
                    db.session.add(entry)
                    db.session.commit()
                    send_email(
                        left_user.email,
                        "Your Partner Registered",
                        "account/email/partner_registered",
                        employee=left_user,
                        partner=user,
                    )
            elif form.parent_email.data:
                # user is child, delete is_partner record if any
                Family.query.filter_by(other=user, is_partner=True).delete()
                left_user = User.query.filter_by(
                    email=form.parent_email.data
                ).first()
                # check if record exists
                entry = Family.query.filter_by(
                    user=left_user, other=user, is_child=True
                ).first()
                # if record does not exist, create it
                if not entry:
                    entry = Family(user=left_user, other=user, is_child=True)
                    db.session.add(entry)
                    db.session.commit()
                    send_email(
                        left_user.email,
                        "Your Child Registered",
                        "account/email/child_registered",
                        employee=left_user,
                        child=user,
                    )
        flash("Account was updated.")
        return redirect(url_for(".details_my_account"))
    else:
        form.firstname.data = user.firstname
        form.lastname.data = user.lastname
        if company:
            form.company_id.data = company.id
        partner = Family.query.filter_by(other=user, is_partner=True).first()
        if partner:
            form.partner_email.data = User.query.get(partner.user_id).email
        parent = Family.query.filter_by(other=user, is_child=True).first()
        if parent:
            form.parent_email.data = User.query.get(parent.user_id).email

    note = """If you work for ESA/EUMETSAT please select the company from the
    list. If you register as family member, only provide the email of your
    partner/parent."""
    return render_template(
        "form.html", title="Update Account", note=note, form=form
    )


@account.route("/deactivate-my-account", methods=["GET", "POST"])
@login_required
def deactivate_my_account():
    form = DeactivateAccountRequestForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.password.data):
            send_email(
                os.environ.get("FEEDBACK_EMAIL"),
                "Request to Deactivate Account",
                "account/email/deactivate_account_request",
                user=current_user,
            )
            logout_user()
            flash(
                "Your request to disable your account has been sent by email."
            )
            return redirect("/")
    note = """Deactivation of your account will disable your login permanently.
    Only deactivate if you are sure you will not use your account anymore.
    An email will be sent to administration to confirm account deactivation."""
    return render_template(
        "form.html", title="Deactive Account", note=note, form=form
    )
