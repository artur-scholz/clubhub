from flask import render_template, flash, redirect
from flask_login import login_required

from clubhub.core import db
from clubhub.utils import superadmin_required, parse_arg
from clubhub.models import User
from .. import account


@account.route("/deactivate-account")
@login_required
@superadmin_required
def deactivate_account():
    user_id = parse_arg("user_id", int)
    user = User.query.get_or_404(user_id)
    user.email = "xxxxxxxxxx" + str(user.id)
    db.session.add(user)
    flash("The user account is now deactivated.")
    return redirect("/")


@account.route("/list-confirmed-users")
@login_required
@superadmin_required
def list_confirmed_users():
    users = User.query.filter_by(confirmed=True).order_by(User.firstname).all()
    return render_template(
        "account/list_users.html", title="Confirmed Users", users=users
    )


@account.route("/list-unconfirmed-users")
@login_required
@superadmin_required
def list_unconfirmed_users():
    users = (
        User.query.filter_by(confirmed=False).order_by(User.firstname).all()
    )
    return render_template(
        "account/list_users.html", title="Unconfirmed Users", users=users
    )
