from flask_wtf import FlaskForm
from flask_login import current_user
from wtforms import (
    StringField,
    SubmitField,
    TextAreaField,
    PasswordField,
    SelectField,
    ValidationError,
)
from wtforms.validators import Length, Regexp, DataRequired, Email, Optional

from clubhub.models import User


class UpdateAccountForm(FlaskForm):
    firstname = StringField(
        "Firstname", validators=[DataRequired(), Length(1, 64)]
    )
    lastname = StringField(
        "Lastname", validators=[DataRequired(), Length(1, 64)]
    )
    company_id = SelectField(
        "Option 1: I work as staff/contractor for:", coerce=int
    )
    partner_email = StringField(
        "Option 2: I am partner of staff/contractor who has following email:",
        validators=[Email(), Optional()],
    )
    parent_email = StringField(
        "Option 3: I am child of staff/contractor who has following email:",
        validators=[Email(), Optional()],
    )
    phone = StringField(
        validators=[
            Length(0, 64),
            Regexp(
                "^\+?[0-9\s]*$",
                0,
                "Phone must have only numbers and country code",
            ),
        ],
        description="(optional)",
    )
    about = TextAreaField(
        "About me", description="(optional)", validators=[Length(max=4000)]
    )
    submit = SubmitField("Update")

    def validate(self):
        if super().validate():
            # make sure that only one of the three fields is filled out
            if (
                self.company_id.data
                and self.partner_email.data
                or self.company_id.data
                and self.parent_email.data
                or all(
                    [
                        self.company_id.data,
                        self.partner_email.data,
                        self.parent_email.data,
                    ]
                )
            ):
                self.company_id.errors.append(
                    "Please choose only one of the three options."
                )
                return False
            if self.partner_email.data and self.parent_email.data:
                self.partner_email.errors.append(
                    "Please choose only one of the three options."
                )
                return False
            # make sure that not all fields are empty
            if not any(
                [
                    self.company_id.data,
                    self.partner_email.data,
                    self.parent_email.data,
                ]
            ):
                self.company_id.errors.append(
                    "Please choose one of the three options."
                )
                return False

            if self.partner_email.data:
                self.partner_email.data = self.partner_email.data.lower()
                if current_user.email == self.partner_email.data:
                    self.partner_email.errors.append(
                        "You must specify the email of your partner, "
                        "not your own."
                    )
                    return False
                user = User.query.filter_by(
                    email=self.partner_email.data
                ).first()
                if not user:
                    self.partner_email.errors.append(
                        "Could not find a user with that email. Either the "
                        "email is wrong or not registered at this website."
                    )
                    return False
                if not user.is_employee():
                    self.partner_email.errors.append(
                        "Your partner is not registered as staff/contractor."
                    )
                    return False

            if self.parent_email.data:
                self.parent_email.data = self.parent_email.data.lower()
                if current_user.email == self.parent_email.data:
                    self.parent_email.errors.append(
                        "You must specify the email of your parent, "
                        "not your own."
                    )
                    return False
                user = User.query.filter_by(
                    email=self.parent_email.data
                ).first()
                if not user:
                    self.parent_email.errors.append(
                        "Could not find a user with that email. Either the "
                        "email is wrong or not registered at this website."
                    )
                    return False
                if not user.is_employee():
                    self.parent_email.errors.append(
                        "Your parent is not registered as staff/contractor."
                    )
                    return False
            return True
        return False


class DeactivateAccountRequestForm(FlaskForm):
    password = PasswordField(validators=[DataRequired()])
    submit = SubmitField()

    def validate_password(self, field):
        if not current_user.verify_password(field.data):
            raise ValidationError("Wrong password.")
