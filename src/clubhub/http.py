from flask import request, jsonify, render_template


def error_response(status_code, message):
    if (
        request.accept_mimetypes.accept_json
        and not request.accept_mimetypes.accept_html
    ):
        response = jsonify(message)
        response.status_code = status_code
        return response
    return render_template(str(status_code) + ".html"), status_code


def register_error_handlers(app):
    @app.errorhandler(400)
    def bad_request(e):
        return error_response(400, {"error": "bad request"})

    @app.errorhandler(401)
    def unauthorized(e):
        return error_response(401, {"error": "unauthorized"})

    @app.errorhandler(403)
    def forbidden(e):
        return error_response(403, {"error": "forbidden"})

    @app.errorhandler(404)
    def page_not_found(e):
        return error_response(404, {"error": "not found"})

    @app.errorhandler(405)
    def method_not_allowed(e):
        return error_response(405, {"error": "method not allowed"})

    @app.errorhandler(500)
    def internal_server_error(e):
        return error_response(500, {"error": "internal server error"})
