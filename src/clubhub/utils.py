from functools import wraps

from flask import abort, request, flash
from flask_login import current_user
from wtforms import ValidationError

from clubhub.models import User, Membership


def get_conflicting_memberships(
    user_id, club_id, valid_from, valid_till, exclude_membership_id=None
):
    existing_memberships = Membership.query.filter_by(
        user_id=user_id, club_id=club_id, confirmed=True
    ).all()
    if not existing_memberships:
        return None
    conflicting_memberships = []
    if exclude_membership_id:
        existing_memberships.remove(
            Membership.query.get(exclude_membership_id)
        )
    for existing_membership in existing_memberships:
        if (
            existing_membership.valid_from <= valid_from
            and existing_membership.valid_till >= valid_from
        ):
            conflicting_memberships.append(existing_membership)
        elif (
            existing_membership.valid_from <= valid_till
            and existing_membership.valid_till >= valid_till
        ):
            conflicting_memberships.append(existing_membership)
        elif (
            existing_membership.valid_from >= valid_from
            and existing_membership.valid_till <= valid_till
        ):
            conflicting_memberships.append(existing_membership)
    return conflicting_memberships


def is_pending_membership(user_id, club_id, valid_from, valid_till):
    return Membership.query.filter_by(
        user_id=user_id,
        club_id=club_id,
        valid_from=valid_from,
        valid_till=valid_till,
        confirmed=False,
    ).first()


# ------------------------------------------------------------------------------
# Validation
# ------------------------------------------------------------------------------


def validate_user_email(self, field):
    field.data = field.data.lower()
    if not User.query.filter_by(email=field.data).first():
        raise ValidationError("No user with that email found.")
        return False
    else:
        return True


def validate_time_range(self, start, end):
    if start > end:
        raise ValidationError('"Valid Till" must be later than "Valid From".')
        return False
    else:
        return True


# ------------------------------------------------------------------------------
# Argument parsing
# ------------------------------------------------------------------------------


def parse_arg(arg, datatype):
    arg = request.args.get(arg)
    if not arg:
        None
    try:
        arg = datatype(arg)
    except Exception:
        None
    return arg


# ------------------------------------------------------------------------------
# Permissions
# ------------------------------------------------------------------------------


def superadmin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if (
            not current_user.is_authenticated
            or not current_user.is_superadmin()
        ):
            abort(401)
        return f(*args, **kwargs)

    return decorated_function


def require_club_admin(user, club):
    if not user.is_club_admin(club) and not user.is_superadmin():
        flash("You must be club admin to see this page.")
        abort(403)


def require_company_admin(user, company):
    if not user.is_company_admin(company) and not user.is_superadmin():
        flash("You must be company admin to see this page.")
        abort(403)


def require_user_admin(user, other):
    if not user.is_admin_of_user(other) and not user.is_superadmin():
        flash("You must be admin to see this page.")
        abort(403)
