import os

from flask import render_template, current_app, flash
from flask_mail import Message
import requests

from .core import mail


def send_email(to, subject, template, **kwargs):
    if os.environ.get("MAIL_VIA_API") and int(os.environ.get("MAIL_VIA_API")):
        return send_email_via_api(to, subject, template, **kwargs)
    elif os.environ.get("MAIL_VIA_SMTP") and int(
        os.environ.get("MAIL_VIA_SMTP")
    ):
        return send_email_via_smtp(to, subject, template, **kwargs)
    flash("Error in sending email.")
    return False


def send_email_via_smtp(to, subject, template, **kwargs):
    app = current_app._get_current_object()
    msg = Message(subject, sender=app.config["MAIL_USERNAME"], recipients=[to])
    msg.body = render_template(template + ".txt", **kwargs)
    msg.html = render_template(template + ".html", **kwargs)
    try:
        with app.app_context():
            mail.send(msg)
        return True
    except Exception as error:
        app.logger.error("Mail via SMTP failed: {}".format(error))
        return False


def send_email_via_api(to, subject, template, **kwargs):
    app = current_app._get_current_object()
    text = render_template(template + ".txt", **kwargs)
    html = render_template(template + ".html", **kwargs)
    response = requests.post(
        os.environ.get("MAIL_API_URL"),
        auth=("api", os.environ.get("MAIL_API_KEY")),
        data={
            "from": os.environ.get("MAIL_API_USERNAME"),
            "to": [to],
            "subject": subject,
            "text": text,
            "html": "<html>" + html + "</html>",
        },
    )
    if response.status_code != 200:
        app.logger.error(
            "Mail via API failed: {}".format(response.json().get("message"))
        )
        return False
    return True
