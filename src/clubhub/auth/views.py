from flask import render_template, redirect, request, url_for, flash
from flask_login import login_user, logout_user, login_required, current_user
from .forms import (
    LoginForm,
    RegistrationForm,
    ChangePasswordForm,
    PasswordResetRequestForm,
    PasswordResetForm,
    ChangeEmailForm,
)

from clubhub.core import db
from clubhub.email import send_email
from clubhub.models import User
from . import auth


@auth.before_app_request
def before_request():
    if current_user.is_authenticated:
        if (
            not current_user.confirmed
            and request.endpoint
            and request.endpoint[:5] != "auth."
            and ".static" not in request.endpoint
        ):
            return redirect(url_for("auth.unconfirmed"))


@auth.route("/unconfirmed")
def unconfirmed():
    if current_user.is_anonymous or current_user.confirmed:
        return redirect("/")
    return render_template("auth/unconfirmed.html")


@auth.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            return redirect(request.args.get("next") or "/")
        flash("Invalid username or password.")
    return render_template("auth/login.html", form=form)


@auth.route("/logout")
@login_required
def logout():
    logout_user()
    flash("You have been logged out.")
    return redirect("/")


@auth.route("/register", methods=["GET", "POST"])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(
            email=form.email.data,
            firstname=form.firstname.data,
            lastname=form.lastname.data,
            password=form.password.data,
        )
        db.session.add(user)
        db.session.commit()
        token = user.generate_confirmation_token()
        email_sent = send_email(
            user.email,
            "Confirm Your Account",
            "auth/email/confirm",
            user=user,
            token=token,
        )
        if email_sent:
            flash(
                "A confirmation email has been sent to you by email. "
                "Please check your email in a few minutes and follow "
                "the instructions on it. You can login to request a new "
                "confirmation email. (Please check your Spam/Junk folder "
                "first!)"
            )
        return redirect(url_for("auth.login"))
    return render_template("auth/register.html", form=form)


@auth.route("/confirm/<token>")
@login_required
def confirm(token):
    if current_user.confirmed:
        return redirect("/")
    if current_user.confirm(token):
        flash("You have confirmed your account. Thanks!")
        return redirect(url_for("account.update_my_account"))
    else:
        flash("The confirmation link is invalid or has expired.")
    return redirect("/")


@auth.route("/confirm")
@login_required
def resend_confirmation():
    token = current_user.generate_confirmation_token()
    email_sent = send_email(
        current_user.email,
        "Confirm Your Account",
        "auth/email/confirm",
        user=current_user,
        token=token,
    )
    if email_sent:
        flash(
            "A new confirmation email has been sent to you by email."
            "Please remember to check your Spam/Junk folder."
        )
    return redirect("/")


@auth.route("/change-password", methods=["GET", "POST"])
@login_required
def change_password():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.password.data
            db.session.add(current_user)
            db.session.commit()
            flash("Your password has been updated.")
            return redirect("/")
        else:
            flash("Invalid password.")
    return render_template("form.html", title="Change Password", form=form)


@auth.route("/reset", methods=["GET", "POST"])
def password_reset_request():
    if not current_user.is_anonymous:
        return redirect("/")
    form = PasswordResetRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            token = user.generate_reset_token()
            email_sent = send_email(
                user.email,
                "Reset Your Password",
                "auth/email/reset_password",
                user=user,
                token=token,
                next=request.args.get("next"),
            )
            if email_sent:
                flash(
                    "An email with instructions to reset your password has "
                    "been sent to you. Please remember to check your "
                    "Spam/Junk folder."
                )
            return redirect(url_for("auth.login"))
        flash("User not found.")
    return render_template("form.html", title="Reset Password", form=form)


@auth.route("/reset/<token>", methods=["GET", "POST"])
def password_reset(token):
    if not current_user.is_anonymous:
        return redirect("/")
    form = PasswordResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None:
            return redirect("/")
        if user.reset_password(token, form.password.data):
            flash("Your password has been updated.")
            return redirect(url_for("auth.login"))
        else:
            return redirect("/")
    return render_template("form.html", title="Reset Password", form=form)


@auth.route("/change-email", methods=["GET", "POST"])
@login_required
def change_email_request():
    form = ChangeEmailForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.password.data):
            new_email = form.email.data
            token = current_user.generate_email_change_token(new_email)
            email_sent = send_email(
                new_email,
                "Confirm your email address",
                "auth/email/change_email",
                user=current_user,
                token=token,
            )
            if email_sent:
                flash(
                    "An email with instructions to confirm your new email "
                    "address has been sent to you. Please remember to check "
                    "your Spam/Junk folder."
                )
            return redirect("/")
        else:
            flash("Invalid email or password.")
    return render_template("form.html", title="Change Email", form=form)


@auth.route("/change-email/<token>")
@login_required
def change_email(token):
    if current_user.change_email(token):
        flash("Your email address has been updated.")
    else:
        flash("Invalid request.")
    return redirect("/")
