from flask import Blueprint

auth = Blueprint(
    "auth", __name__, template_folder="templates", static_folder="static"
)

from . import views

from clubhub.core import login_manager
from clubhub.models import User


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
