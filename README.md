# ClubHub

A web application powered by [Flask](http://flask.pocoo.org) for club and
membership management.

##  Installation

### Option 1: Manual Installation (development)

To install ClubHub on your machine, follow the instructions below. Note that
for the database it will simply use a file (SQLite format).

Clone or download the repository to your computer.

```
$ git clone https://gitlab.com/artur-scholz/clubhub.git
$ cd clubhub
$ python3 -m venv venv
$ . venv/bin/activate
$ pip install -e .
```

Then copy the *.env_template* file to *.env* and make adjustments as needed.

```
$ cp .env_template .env
$ vim .env
```

And you're set! To start the server on Linux, run the following commands:

```
$ export FLASK_APP=clubhub
$ flask run
```

To start the server on Windows, run the following commands:

```
$ set FLASK_APP=clubhub
$ flask run
```

To verify that ClubHub is running open your browser and visit http://localhost:5000.

### Option 2: Docker Installation (development)

As an alternative to the manual installation described above you can run ClubHub in
a docker container. To do so, you first need to have installed docker and
docker-compose on your machine.

Clone or download the repository to your computer. Enter the docker directory.

Run `docker-compose -f docker-compose.dev.yml up`.

To verify that ClubHub is running open your browser and visit http://localhost:5000.

### Option 3: Docker Installation (production)

For a live website it is advised to use the production-ready Docker container.

Clone or download the repository to your computer. Enter the docker directory
and edit the `docker-compose.live.yml` file to match your configuration.

First, request a certificate:
1. Make sure you have a public accessible domain, in order to request a LetsEncrypt
security certificate for secure HTTPS access.
2. Set your domain name in files:
  - *docker/etc/nginx/default.conf*
  - *docker/docker-compose.cert.yml*
3. Run `docker-compose -f docker-compose.cert.yml up`. The certificate will be
generated.

Then run `docker-compose -f docker-compose.live.yml up -d` to start the server.

To verify that ClubHub is running open your browser and visit http://your-domain.

## Initialize Database

You will need to initialize the database and create the admin account by hand
after a fresh install (applies to both, bare install and docker).

This first step is needed only if you use a docker container:

```
docker-compose -f docker-compose.xxx.yml run clubhub bash
```

Initialize and populate the database:

```
$ flask db init
$ flask db migrate
$ flask db upgrade
```

Then create the admin user. This is done in the flask shell.

```python
$ flask shell
from clubhub.models import User, SuperAdmin
from clubhub.core import db
user = User(email='admin@local.host', password='a-good-password-please')
user.confirmed = True
db.session.add(user)
admin = SuperAdmin()
admin.user = user
db.session.add(admin)
db.session.commit()
exit()
$ exit
```

## Testing

After changes to the code and before submitting merge requests, please run
the unit tests (install the test suite beforehand: `pip install coverage pytest`):

```
$ coverage run -m pytest .
```
